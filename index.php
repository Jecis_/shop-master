<!DOCTYPE html>
<html lang="en">

    <?php
    $title = 'Sākums'; ?>
    <?php $currentPage = 'index'; ?>
    <?php include('navbar.php'); ?>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $product = New \aptieka\controllers\ActionController();
        $product->addSubs();
    }
    ?>
<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Vislabākais no dabas Tev!</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-like m-auto text-success" onclick="window.location.href='aktualitates.php'"></i>
                    </div>
                    <h3>Aktualitātes</h3>
                    <p class="lead mb-0">Jaunumi, akcijas, piedāvājumi!</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-basket m-auto text-success" onclick="window.location.href='produkti.php'"></i>
                    </div>
                    <h3>Produktu klāsts</h3>
                    <p class="lead mb-0">Pašlaik pieejamie produkti mūsu aptiekā.</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-check m-auto text-success" onclick="window.location.href='ieteikumi.php'"></i>
                    </div>
                    <h3>Ekspertu ieteikumi</h3>
                    <p class="lead mb-0">Idejas un ieteikumi veselības saudzēšanai.</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon d-flex">
                        <i class="icon-phone m-auto text-success" onclick="window.location.href='kontakti.php'"></i>
                    </div>
                    <h3>Kontakti</h3>
                    <p class="lead mb-0">Mūsu aptieku tīkla kontaktinformācija.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="innersection text-white text-center">
    <div class="overlay"></div>
</section>
<!-- Image Showcases -->
<section class="showcase">
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/A2270732-p1.png');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Pret saaukstēšanos</h2>
                <p class="lead mb-0">Aerosoli aizliktam degunam, kā arī sīrupi un uzlējumi pret klepu.</p>
                <div class="btn btn-success mt-3" onclick="window.location.href='produkti.php'">Uzzināt vairāk</div>
            </div>
        </div>
        <section class="innersection text-white text-center">
            <div class="overlay"></div>
        </section>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('img/8a1004141db83c8eaab3ba4d1da37251.jpg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <h2>Pašiem mazākajiem</h2>
                <p class="lead mb-0">Līdzekļi bērnu un zīdaiņu imunitātes uzlabošanai, pret sāpēm zobu šķilšanās laikā,
                    miega traucējumu gadījumā un alerģiju simptomu mazināšanai.</p>
                <div class="btn btn-success mt-3" onclick="window.location.href='produkti.php'">Uzzināt vairāk</div>
            </div>
        </div>
        <section class="innersection text-white text-center">
            <div class="overlay"></div>
        </section>
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/pumpan_48_tablets_dystonia_treatment__1.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Lai sirds būtu stipra!</h2>
                <p class="lead mb-0">Plašs līdzekļu klāsts sirds un asinsvadu slimību ārstēšanai, kā arī citu iekšējo
                    orgānu darbības uzlabošanai.</p>
                <div class="btn btn-success mt-3" onclick="window.location.href='produkti.php'">Uzzināt vairāk</div>
            </div>
        </div>
    </div>
</section>
<section class="innersection text-white text-center">
    <div class="overlay"></div>
</section>
<!-- Testimonials -->
<section class="testimonials text-center bg-light">
    <div class="container-fluid">
        <h2 class="mb-5">Jaunākie produktu zīmoli mūsu klāstā</h2>
        <div class="row">
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/boiron.png" alt="">
                    <h5>BOIRON</h5>
                    <p class="font-weight-light mb-0">Vadošais homeopātisko līdzekļu ražotājs ASV</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/heel.gif" alt="">
                    <h5>HEEL</h5>
                    <p class="font-weight-light mb-0">Kompānija, kura ražo medikamentus no dabīgām sastāvdaļām</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/Logo_DHU.svg.png" alt="">
                    <h5>DHU</h5>
                    <p class="font-weight-light mb-0">Lielākais homeopātisko līdzekļu ražotājs Vācijas tirgū ar 150 gadu
                        pieredzi</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                      method="post">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg"
                                   placeholder="Lūdzu, ievadi e-pasta adresi..." required name="email">
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
