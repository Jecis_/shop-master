<?php

namespace aptieka\database;


use aptieka\database\seeders\ProductSeeder;
use aptieka\database\seeders\RoleSeeder;
use aptieka\database\seeders\UsersSeeder;
use aptieka\database\seeders\NewsSeeder;
require_once '../../autoload.php';

Class SetupDb extends Database
{
    public function createTables()
    {
//  connect to database
        $connectionDB = $this->connection;

//  Run SQL Ouery to Create tables
        try {
            $sqlTables = new tables();
            $connectionDB->query($sqlTables->Users());
            $connectionDB->query($sqlTables->roles());
            $connectionDB->query($sqlTables->permissions());
            $connectionDB->query($sqlTables->role_perm());
            $connectionDB->query($sqlTables->userRoles());

            $connectionDB->query($sqlTables->Product());
            $connectionDB->query($sqlTables->addProductToOrder());
            $connectionDB->query($sqlTables->Orders());
            $connectionDB->query($sqlTables->News());
            $connectionDB->query($sqlTables->addSubs());
            if (!$connectionDB->query($sqlTables->News())) {
                $connectionDB->error;
            }

            echo "  Tables created";
            echo $connectionDB->error;

        } catch (\Exception $e) {
            echo 'ERROR: ' . $e;
        }
        $connectionDB->close();
    }

// Insert data in tables - run seeders
    public function databaseSeeder()
    {
        try{
            new NewsSeeder();
        }catch (\mysqli_sql_exception $error){
            print_r($error);
        }

        try{
            new ProductSeeder();
        }catch (\Error $error){
            print_r($error);
        }

        try{
            new UsersSeeder();
        }catch (\mysqli_sql_exception $error){
            print_r($error);
        }
      try{
          new RoleSeeder();
        }catch (\mysqli_sql_exception $error){
            print_r($error);
        }


    }
}

try{
    new CreateDb();
}catch (\Error $error){
    print_r($error);
}
$createDatabase = new SetupDb();

try{
    $createDatabase->createTables();
}catch (\Error $error){
    print_r($error);
}
$createDatabase->databaseSeeder();

