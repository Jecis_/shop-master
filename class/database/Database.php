<?php

namespace aptieka\database;
//require_once '../../autoload.php';

class Database
{

    protected $result;
    protected $numrows;
    protected $connection;



    /*Protected database configuration
    you need to set your database variables in
    /src/config/config.ini*/

    public function __construct()
    {

        //retrieve config variables
       /* $config = $this->config();*/
        //create new connection
        $this->connection = new \mysqli("localhost", 'root', 'password','shop');
        //catch errors
        mysqli_set_charset($this->connection, "utf8mb4") ;
        /* change character set to utf8 */
       /*if (!mysqli_set_charset($this->connection, "utf8mb4")) {
            printf("Error loading character set utf8: %s\n", mysqli_error($this->connection));
            exit();
        }*/
        if ($this->connection === false) {
            die("ERROR:Connection Failed " . $this->connection->connect_error);
        }
        //return connection
        return $this->connection;
    }

    public function getProperty($name)
    {
        return $this->$name;
    }

    /**
     * @param $name
     * @param $val
     * @return mixed
     */
    public function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    /**
     * @return array|bool
     */
    /*protected function config()
    {
        //retrieve config variables
        $config = parse_ini_file(__DIR__ . '/config.ini');
        return $config;
    }*/

    /**
     * @return array|bool
     */


    /**
     * @param $table
     * @return bool|\mysqli_result
     */
    public function selectTableData($table)
    {
        $sql = "SELECT * FROM " . $table;
        $result = $this->connection->query($sql);

// Fetch all
        if ($result->num_rows === 0) {
        } else {
            while ($row = $result->fetch_assoc()) {
                $results[] = $row;
                $this->setProperty('result', $results);
            }
        }
        $this->connection->close();
        return $result;
    }

    /**
     * @param $table
     * @param $select
     * @param $column
     * @param $action
     * @param $data
     */
    public function whereQuery($table, $select, $column, $action, $data)
    {

        $sql = 'SELECT ' . $select . ' FROM ' . $table . ' WHERE ' . $column . ' ' . $action . ' ?';
        $stmt = $this->connection->prepare($sql);
        if ($stmt) {
            $stmt->bind_param('s', $data);
            $stmt->execute();
            $res = $stmt->get_result();
            $stmt->close();
        }
        if ($res->num_rows === 0) {
        } else {
            while ($row = $res->fetch_assoc()) {
                $results[] = $row;
                $this->setProperty('result', $results);
            }
        }
        $this->connection->close();
        return;
    }
    public function whereQueryObj($table, $select, $column, $action, $data)
    {
    //var_dump($table);

        $sql = 'SELECT ' . $select . ' FROM ' . $table . ' WHERE ' . $column . ' ' . $action . ' ?';
        $stmt = $this->connection->prepare($sql);
        if ($stmt) {
            $stmt->bind_param('s', $data);
            $stmt->execute();
            $res = $stmt->get_result();
            $stmt->close();
        }
        if ($res->num_rows === 0) {
        } else {
            while ($row = $res->fetch_object()) {
                $results = $row;
                $this->setProperty('result', $results);
            }
        }
        $this->connection->close();
        return;
    }

    /**
     * @param $table
     * @param $dbfields
     * @param $types
     * @param $column
     * @param $action
     * @param $data
     * @return bool
     */
    public function update($table, $dbfields, $types, $column, $action, $data)
    {
        /**
         * possible types: i, d, s and b, which stand for integer, double, string and binary.
         */
        $questionMarks = [];
        foreach ($dbfields as $DB) {
            $questionMarks[] = $DB . ' = ?';
        }

        $values = implode(",", $questionMarks);

        $sql = 'UPDATE ' . $table . ' SET ' . $values . ' WHERE ' . $column . ' ' . $action . ' ?';
        if ($stmt = $this->connection->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
            $a_params = array();
            $param_type = '';
            $n = count($types);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $types[$i];
            }
            /* with call_user_func_array, array params must be passed by reference */
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                /* with call_user_func_array, array params must be passed by reference */
                $a_params[] = &$data[$i];
            }
            /* use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array */

            call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();

            /* Execute statement */

            $this->connection->close();
            return true;
        } else {
            trigger_error($this->connection->error, E_USER_ERROR);
            $this->connection->close();
            return false;

        }
    }

    public function customQuery($sql, $data, $types)
    {
        $stmt = $this->connection->prepare($sql);
        if ($stmt) {
            $stmt->bind_param($types, $data);
            $stmt->execute();
            $res = $stmt->get_result();
            $stmt->close();
        }
        if ($res->num_rows === 0) {
        } else {
            while ($row = $res->fetch_assoc()) {
                $results[] = $row;
                $this->setProperty('result', $results);
            }
        }
        $this->connection->close();
        return true;

    }

    /**
     * @param $table
     * @param $dbfields
     * @param $types
     * @param $data
     */
    public function insert($table, $dbfields, $types, $data)
    {
        /**
         * possible types: i, d, s and b, which stand for integer, double, string and binary.
         */
        $questionMarks = [];
        foreach ($dbfields as $DB) {
            $questionMarks[] = '?';
        }
        $values = implode(",", $questionMarks);
        $dbInsertInto = implode(",", $dbfields);

        $sql = "INSERT INTO " . $table . " (" . $dbInsertInto . ") VALUES (" . $values . ")";
        if ($stmt = $this->connection->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            /* Bind parameters. Types: s = string, i = integer, d = double,  b = blob */
            $a_params = array();
            $param_type = '';
            $n = count($types);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $types[$i];
            }
            /* with call_user_func_array, array params must be passed by reference */
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                /* with call_user_func_array, array params must be passed by reference */
                $a_params[] = &$data[$i];
            }
            /* use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array */
            call_user_func_array(array($stmt, 'bind_param'), $a_params);
            /* Execute statement */
            $stmt->execute();
            $this->connection->close();
        } else {
            echo 'Error';
           trigger_error($this->connection->error, E_USER_ERROR);
            return;
        }

    }

    public function drop($table)
    {
        $sql = 'TRUNCATE ' . $table;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $this->connection->close();

    }

    public function deleteRow($table, $param)
    {
        $sql = 'DELETE FROM '.$table.' WHERE id =' . $param;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $stmt->close();
        $this->connection->close();

    }
}
