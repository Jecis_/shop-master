<?php
/**
 * Created by PhpStorm.
 * User: jecis
 * Date: 3/9/2019
 * Time: 01:02
 */
namespace aptieka\database;

class CreateDb
{
    public function __construct()
    {
//  Retrieve  config values

//  Create connection to localHost username and password variables needs to be defined in /src/config/config.ini
        $connectToDatabase = new \mysqli("localhost", 'root', 'password');

//  Create database or retrieve errors
        $Create = "CREATE DATABASE shop CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
        if ($connectToDatabase->query($Create)) {
            echo "Database created successfully";
        } else {
            echo "ERROR: Could not able to execute $Create. " . $connectToDatabase->error;
        }
        $connectToDatabase->close();
    }
}