<?php
/**
 * Created by PhpStorm.
 * User: jecis
 * Date: 3/9/2019
 * Time: 00:21
 */
namespace aptieka\database\seeders;
use aptieka\auth\PrivilegedUser;
use aptieka\auth\Role;
use aptieka\models\User;
class RoleSeeder{
    public function __construct()
    {
        $this->roleSeeder();
        $this->permissionSeeder();
        $this->rolePermSeeder();
        $this->userPermSeeder();
    }

    public function roleSeeder()
    {
        Role::insertRole(['admin']);
        Role::insertRole(['user']);
        Role::insertRole(['editor']);
        Role::insertRole(['productManager']);
        echo '<br> Roles Seeded';
    }
    public function permissionSeeder()
    {
        PrivilegedUser::insertPermission(['allow']);
        PrivilegedUser::insertPermission(['view']);
        PrivilegedUser::insertPermission(['writeNews']);
        PrivilegedUser::insertPermission(['addProducts']);
        PrivilegedUser::insertPermission(['editProducts']);
        PrivilegedUser::insertPermission(['deleteProducts']);
        echo '<br> Permissions Seeded';
    }

    public function rolePermSeeder()
    {
        PrivilegedUser::insertPerm('1', '1');
        PrivilegedUser::insertPerm('2', '2');
        PrivilegedUser::insertPerm('3', '3');
        PrivilegedUser::insertPerm('4', '4');
        echo '<br> Role Permissions pivot Seeded';
    }

    public function userPermSeeder()
    {
        $user_id = User::byEmail('admin@admin.com');
        Role::insertUserRoles($user_id, '1');
        $user_id = User::byEmail('user@user.com');
        Role::insertUserRoles($user_id, '2');
        $user_id = User::byEmail('editor@editor.com');
        Role::insertUserRoles($user_id, '3');
        $user_id = User::byEmail('Product@Manager.com');
        Role::insertUserRoles($user_id, '4');
        echo '<br> User Roles Seeded';

    }
}