<?php

namespace aptieka\database\seeders;

use aptieka\models\Product;
use aptieka\Helpers;

class ProductSeeder extends Product
{
    public function __construct()
    {
        parent::__construct();

        $product = new Product();
        $product->name = 'GRIPP-HEEL TABLETES';
        $product->price = '6.45';
        $product->discount = '0';
        $product->category = 'Pret saaukstēšanos';
        $product->about = 'Homeopātiskās zāles gripas un gripveida infekcijas ārstēšanai.';
        $product->prescription = '0';
        $product->picture = 'https://i.ebayimg.com/images/i/332616229484-0-1/s-l1000.jpg';
        $product->insert(Helpers::objToArr($product));

        $product = new Product();
        $product->name = 'INFLUCID GTT PILIENI';
        $product->price = '11.97';
        $product->discount = '0';
        $product->category = 'Pret saaukstēšanos';
        $product->about = 'Influcid ir homeopātiskās zāles saaukstēšanās un drudža, piemēram, gripai.';
        $product->prescription = '0';
        $product->picture = 'https://apteka.lv/pict-product/C.lt/influcid-gtt-30-ml.jpg';
        $product->insert(Helpers::objToArr($product));

        $product = new Product();
        $product->name = 'OSCILLOCOCCINUM ZIRNĪŠI';
        $product->price = '6.31';
        $product->discount = '10';
        $product->category = 'Pret saaukstēšanos';
        $product->about = 'Oscillococcinum – homeopātiskās zāles gripas un tās simptomu ārstēšanai.';
        $product->prescription = '0';
        $product->picture = 'https://www.soin-et-nature.com/15273/boiron-homeopathic-oscillococcinum-30-doses.jpg';
        $product->insert(Helpers::objToArr($product));

        $product = new Product();
        $product->name = 'GASTRICUMEEL TABLETES';
        $product->price = '6.45';
        $product->discount = '5';
        $product->category = 'Gremošanas sistēma';
        $product->about = 'Gastricumeel tabletes ir homeopātiskas zāles, ko lieto akūta un hroniska gastrīta, grēmu, meteorisma gadījumā.';
        $product->prescription = '0';
        $product->picture = 'https://cdn1.apopixx.de/400/web_schraeg/00407641.jpg';
        $product->insert(Helpers::objToArr($product));

        $product = new Product();
        $product->name = 'STIRAN PILIENI';
        $product->price = '9.12';
        $product->discount = '0';
        $product->category = 'Pret saaukstēšanos';
        $product->about = 'Deguna blakusdobumu iekaisuma, hronisku iesnu un hroniska laringotraheīta ārstēšanai. ';
        $product->prescription = '0';
        $product->picture = 'http://omega-pharma.lv/uploads/products/p3_stiran.jpg';
        $product->insert(Helpers::objToArr($product));
        echo '<br> Products Seeded';

    }

}