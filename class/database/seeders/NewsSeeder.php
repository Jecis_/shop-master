<?php
/**
 * Created by PhpStorm.
 * User: jecis
 * Date: 3/9/2019
 * Time: 00:23
 */

namespace aptieka\database\seeders;

use aptieka\models\News;
use aptieka\Helpers;

class NewsSeeder extends News
{
    public function __construct()
    {
        parent::__construct();

        $news = new News();
        $news->text = 'D vitamīns ir top vitamīns, kas Latvijas platuma grādos trūkst teju ikvienam, turklāt bieži cilvēki to nemaz neapzinās, jo ne vienmēr tā deficīts ir fiziski jūtams. Taču pēdējo divu gadu laikā novērota pozitīva tendence – pieprasījums pēc D vitamīna palielinājies vairāk nekā divas reizes, turklāt cilvēki par D vitamīna līmeni sāk rūpēties savlaicīgi, nevis tad, kad jau konstatēts tā deficīts.';
        $news->picture = 'https://www.apotheka.lv/media/blog/732x448/Saule3.jpg';
        $news->title = 'Aptiekās divkāršojies pieprasījums pēc D vitamīna';
        $news->insert(Helpers::objToArr($news));

        $news = new News();
        $news->text = 'Šonedēļ īpašas cenas lūpu balzāmiem. Cīnāmies pret vēja sapūstām lūpām!';
        $news->picture = 'http://www.menessaptieka.lv/upload/rk/757/75725683bbb13305ec970e8d52b4c9b3.jpg';
        $news->title = 'Lūpu balzāmiem -50%';
        $news->insert(Helpers::objToArr($news));

        $news = new News();
        $news->text = 'Vīrusu un saaukstēšanās sezonā arvien aktuālāk kļūst imunitātes stiprināšanas un profilakses jautājums.';
        $news->picture = 'https://www.benu.lv/files/content/benu-web-dazadi5.jpg';
        $news->title = 'Jauna pieeja vīrusu identificēšanā un ārstēšanā – "1,2,3! Esi vesels drīz!';
        $news->insert(Helpers::objToArr($news));

        $news = new News();
        $news->text = 'Veselīgs miegs ir neatņemama stipras imunitātes sastāvdaļa. Astoņas stundas naktsmiera cilvēkam ļauj justies enerģiskam un produktīvam, turklāt neatsverami uzlabo arī garastāvokli.';
        $news->picture = 'https://www.benu.lv/files/content/labots-sleep-well-gulta-benu-zurnalam-190x260.jpg';
        $news->title = 'Uzdāvini sev Ziemassvētkos labu miegu!';
        $news->insert(Helpers::objToArr($news));
        echo '<br> News Seeded';
    }
}

