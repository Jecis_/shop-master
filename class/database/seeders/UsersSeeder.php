<?php

namespace aptieka\database\seeders;

use aptieka\models\User;
use aptieka\Helpers;

class UsersSeeder extends User
{
    public function __construct()
    {
        parent::__construct();

        $user = new User();
        $user->name = 'admin';
        $user->surname = 'admin';
        $user->email = 'admin@admin.com';
        $user->address = 'test';
        $user->personal_code = '123456-12345';
        $user->password = 'password';
        $user->is_active = 0;
        $user->Validation();
        $user->insert(Helpers::objToArr($user));

        $user = new User();
        $user->name = 'user';
        $user->surname = 'user';
        $user->email = 'user@user.com';
        $user->address = 'test';
        $user->personal_code = '123456-12345';
        $user->password = 'password';
        $user->is_active = 0;
        $user->Validation();
        $user->insert(Helpers::objToArr($user));

        $user = new User();
        $user->name = 'editor';
        $user->surname = 'editor';
        $user->email = 'editor@editor.com';
        $user->address = 'test';
        $user->personal_code = '123456-12345';
        $user->password = 'password';
        $user->is_active = 0;
        $user->Validation();
        $user->insert(Helpers::objToArr($user));

        $user = new User();
        $user->name = 'ProductManager';
        $user->surname = 'ProductManager';
        $user->email = 'Product@Manager.com';
        $user->address = 'test';
        $user->personal_code = '123456-12345';
        $user->password = 'password';
        $user->is_active = 0;
        $user->Validation();
        $user->insert(Helpers::objToArr($user));

        echo '<br> Users Seeded';

    }

}