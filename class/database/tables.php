<?php

namespace aptieka\database;
class tables
{
    public function Users()
    {
        $users = "CREATE TABLE users(
       user_id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
       name VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL,
       surname VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL,
       email VARCHAR(255) NOT NULL,
       address VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL,
       
       personal_code VARCHAR(255) NOT NULL,
       password VARCHAR(255) NOT NULL,
       is_active TINYINT(1) NOT NULL DEFAULT 0
)";

        return $users;
    }

    public function roles()
    {
        $roles =
            "CREATE TABLE roles (
  role_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  role_name VARCHAR(50) NOT NULL,
  PRIMARY KEY (role_id)
)";
        return $roles;
    }

    public function permissions()
    {
        $permissions = "CREATE TABLE permissions(
        perm_id INT UNSIGNED NOT NULL  AUTO_INCREMENT,
  perm_desc VARCHAR(50) NOT NULL,

  PRIMARY KEY(perm_id)
)";
        return $permissions;
    }

    public function role_perm()
    {
        $role_perm = "CREATE TABLE role_perm(
          role_id INT UNSIGNED NOT NULL,
          perm_id INT UNSIGNED NOT NULL,
        
          FOREIGN KEY(role_id) REFERENCES roles(role_id),
          FOREIGN KEY(perm_id) REFERENCES permissions(perm_id)
)";
        return $role_perm;
    }

    // Sql query For  tables
    public function userRoles()
    {
        $userRoles = "CREATE TABLE user_role(
        user_id INT UNSIGNED NOT NULL,
        role_id INT UNSIGNED NOT NULL,
        FOREIGN KEY(user_id) REFERENCES users(user_id),
        FOREIGN KEY(role_id) REFERENCES roles(role_id)
)";
        return $userRoles;
    }


    public function Product()
    {
        $product =
            "CREATE TABLE product(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    price DECIMAL(8, 2) NOT NULL,
    discount INT,
    category TEXT(16383) COLLATE utf8mb4_unicode_ci NOT NULL,
    about TEXT(16383) COLLATE utf8mb4_unicode_ci NOT NULL,
    prescription TINYINT(1) NOT NULL,
    picture VARCHAR(255) NOT NULL
)";
        return $product;
    }

    public function addProductToOrder()
    {
        $add_product_to_orders =
            "CREATE TABLE add_product_to_orders(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    product_id int NOT NULL,
    user_id INT NOT NULL,
    is_active TINYINT(1)
)";

        return $add_product_to_orders;
    }

    public function addSubs()
    {
        $subs =
            'CREATE TABLE subs(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    time DATETIME NOT NULL,
    is_active TINYINT(1)
)';

        return $subs;
    }

    public function Orders()
    {
        $orders = "CREATE TABLE orders(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    order_data TEXT(16383) COLLATE utf8mb4_unicode_ci NOT NULL,
    user_id INT NOT NULL

)";
        return $orders;
    }

    public function News()
    {
        $news = "CREATE TABLE news(
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    text TEXT(16383) COLLATE utf8mb4_unicode_ci NOT NULL,
    picture VARCHAR(255) NOT NULL,
    title TEXT(16383) COLLATE utf8mb4_unicode_ci NOT NULL
)";
        return $news;
    }
}