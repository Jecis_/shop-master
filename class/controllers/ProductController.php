<?php

namespace aptieka\controllers;

use aptieka\models\Product;
use aptieka\Helpers;

class ProductController
{
    public $errors;


    public function addProduct()
    {
        $product = new Product();
        $product->setProperty('name', $_REQUEST['name']);
        $product->setProperty('price', $_REQUEST['price']);
        $product->setProperty('discount', $_REQUEST['discount']);
        $product->setProperty('category', $_REQUEST['category']);
        $product->setProperty('about', $_REQUEST['about']);
        $product->setProperty('prescription', $_REQUEST['prescription']);
        $product->setProperty('picture', $_REQUEST['picture']);
        $product->Validation();

        if (isset($product->errors)) {
            return $this->errors = $product->errors;
        } else {

            $product->insert(Helpers::objToArr($product));
        }
    }

    public function getByCategory()
    {
        $categoryProduct = Product::find($_REQUEST['cat_prod'], 'category');
        return $categoryProduct;
    }

    public static function getAllProducts()
    {
        Product::getAll();
    }

    public function updateProduct()
    {

        $product = Product::Param($_REQUEST['id']);
        $product->setProperty('name', $_REQUEST['name']);
        $product->setProperty('price', $_REQUEST['price']);
        $product->setProperty('discount', $_REQUEST['discount']);
        $product->setProperty('category', $_REQUEST['category']);
        $product->setProperty('about', $_REQUEST['about']);
        $product->setProperty('prescription', $_REQUEST['prescription']);
        $product->setProperty('picture', $_REQUEST['picture']);

        $product->Validation();

        if (isset($product->errors)) {
            return $this->errors = $product->errors;
        } else {
            $product->update(Helpers::objToArr($product));
        }
    }

    public function deleteProduct()
    {

        $val = htmlspecialchars($_REQUEST["id"]);
        Product::delete($val);

    }


}