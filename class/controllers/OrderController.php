<?php


namespace aptieka\controllers;

use aptieka\models\Order;
use aptieka\Helpers;
use aptieka\models\Product;

class OrderController
{
    public function Order_actions()
    {
        if (Order::Param($_REQUEST['user_id']) !== false) {
            $order = Order::Param($_REQUEST['user_id']);
            $order->order_data = $order->order_data . ',' . $_REQUEST['action'];
            $order->Validation();

            if (isset($order->errors)) {
                var_dump($order->errors);
            } else {
                $order->update(Helpers::objToArr($order));
            }
            return header("Status: 200");
        } else {
            $order = new Order();
            $order->order_data = $_REQUEST['action'];
            $order->user_id = $_REQUEST['user_id'];
            $order->Validation();

            if (isset($order->errors)) {
                var_dump($order->errors);
            } else {

                $order->insert(Helpers::objToArr($order));
            }
            return header("Status: 200");
        }
    }

    public function cart()
    {
        $user_id = $_SESSION['user'];

        $user_order = Order::Param($user_id);

        $products[] = explode(',', $user_order->order_data);

        $same_values = array_count_values($products[0]);

        foreach ($same_values as $key => $value) {
            if ($value > 1) {
                $real_values[$key] = $value;
            }
        }
        $result = [];
        $temp = false;
        $repp = 0;
        foreach ($products[0] as $value) {
            // var_dump($prod);
            $products_obj = Product::Param($value);
            $products_obj = $array = json_decode(json_encode($products_obj), true);
            if (isset($real_values[$value])) {
                if ($temp !== true) {
                    $products_obj['count'] = $real_values[$value];
                    $result[] = $products_obj;
                    $temp = true;
                    $repp = $value;
                }
                if ($repp !== $value) {
                    $temp = false;
                }
            } else {
                $products_obj['count'] = 1;
                $result[] = $products_obj;

            }
        }

        return $result;


    }

}