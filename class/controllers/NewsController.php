<?php

namespace aptieka\controllers;

use aptieka\models\News;
use aptieka\Helpers;

class NewsController
{
    public $errors;

    public function addNews()
    {
        $news = new News();
        $news->setProperty('text', $_REQUEST['text']);
        $news->setProperty('picture', $_REQUEST['picture']);
        $news->setProperty('title', $_REQUEST['title']);
        $news->Validation();

        if (isset($news->errors)) {
            return $this->errors = $news->errors;
        } else {

            $news->insert(Helpers::objToArr($news));
        }
    }

    public static function getNews()
    {
        News::init();
        return News::getAll();
    }


    public function updateNews()
    {
        $news = News::byID($_REQUEST['id']);
        $news->setProperty('text', $_REQUEST['text']);
        $news->setProperty('picture', $_REQUEST['picture']);
        $news->setProperty('title', $_REQUEST['title']);
        $news->Validation();

        if (isset($news->errors)) {
            return $this->errors = $news->errors;
        } else {
            $news->insert(Helpers::objToArr($news));
        }
        //
    }

    public function deleteNews()
    {

        $val = htmlspecialchars($_REQUEST["id"]);
        News::delete($val);

    }


}