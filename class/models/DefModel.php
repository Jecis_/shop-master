<?php

namespace aptieka\models;
interface DefModel
{
    public function __construct();

    public static function init();

    public function Validation();
}