<?php

namespace aptieka\models;


use aptieka\validation\Validate;
use aptieka\core\Model;

Class User extends Model implements DefModel
{

    protected $name;
    protected $surname;
    protected $email;
    protected $address;
    protected $personal_code;
    protected $password;
    protected $is_active;
    protected $fields;
    protected $types;
    protected $data;
    protected $errors;

    public function __construct()
    {
        User::init();
    }
    public static function init()
    {
        Model::$table = 'users';
        Model:: $action = '=';
        Model:: $defdata = [
            'table' => 'users',
            'dbfields' => [
                'name', 'surname', 'email', 'address', 'personal_code', 'password', 'is_active'
            ],
            'types' => [
                's', 's', 's', 's', 's', 's', 'i'
            ],
            'typesWhere' => [
                's', 's', 's', 's', 's', 's', 'i', 'i'
            ],
        ];
    }

    public function Validation()
    {
        $validate = new Validate();
        $validate->Validation($this->name, '/^[a-zA-zāčēģīķļņšūžĀČĒĢĪĶĻŅŠŪŽ]+$/');
        $validate->getProperty('state') == 'true' ? $this->name : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->surname, '/^[a-zA-zāčēģīķļņšūžĀČĒĢĪĶĻŅŠŪŽ]+$/');
        $validate->getProperty('state') == 'true' ? $this->surname : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->email, 'email');
        $validate->getProperty('state') == 'true' ? $this->email : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $this->address;

        $validate->Validation($this->personal_code, '/^[0-9]{1,6}[-0-9]{1,6}$/');
        $validate->getProperty('state') == 'true' ? $this->personal_code : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        $this->is_active;
    }

    public static function byEmail($param)
    {
        User::init();
        Model::$col = 'email';
        Model::$select = '*';
        Model::$tableName = get_class();

        return Model::getByParam($param);
    }

    public static function byIsActive($param)
    {
        User::init();
        Model::$col = 'user_id';
        Model::$select = '*';
        Model::$tableName = get_class();
        return Model::getByParam($param);
    }

}

