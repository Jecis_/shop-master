<?php

namespace aptieka\models;

use aptieka\validation\Validate;
use \aptieka\database\Database;

class Subs
{
    protected $table;
    protected $types;
    protected $dbfields;
    protected $email;
    protected $time;
    protected $isactive;

    protected $allSubs;

// Dat vērt
    public function __construct($email)
    {
        $validate = new Validate();

        $validate->Validation($email, 'email');
        $validate->getProperty('state') == 'true' ? $this->email = $email : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $this->time = (new \DateTime())->format('Y-m-d H:i:s');

        $this->isactive = '1';


    }

    public function getProperty($name)
    {
        return $this->$name;
    }

    public function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    public static function defData()
    {
        $statArr = [
            'table' => 'subs',
            'dbfields' => [
                'email', 'time', 'is_active'
            ],
            'types' => [
                's', 's', 'i'
            ],
            'typesWhere' => [
                's', 's', 'i', 'i'
            ]

        ];
        return $statArr;

    }

    public function insertProduct()
    {
        $staticDat = $this->defData();
        $data =
            [
                $this->getProperty('email'),
                $this->getProperty('time'),
                $this->getProperty('isactive')
            ];
        $this->setProperty('data', $data);
        $conn = new Database();
        $conn->insert($staticDat['table'], $staticDat['dbfields'], $staticDat['types'], $data);
    }

    public static function getAllProducts()
    {
        $conn = new Database();
        $conn->selectTableData('subs');
        return $conn->getProperty('result');
    }

    public static function updateProduct($fields, $types, $data)
    {
        $conn = new Database();
        //update fn in data first insert data then where param
        $conn->update('subs', $fields, $types, 'id', '=', $data);
    }


}