<?php
namespace aptieka\models;
use aptieka\validation\Validate;
use \aptieka\database\Database;
class Attributes
{
    protected $table;
    protected $types;
    protected $dbfields;
    protected $name;
    protected $value;


    public function __construct(){


    }
    public function getProperty($name)
    {
        return $this->$name;
    }

    public  function setProperty($name, $val)
    {
        return $this->$name = $val;
    }
public function validate($prg , $name, $value){
    $validate = new Validate();

    $validate->Validation($value,$prg);
    $validate->getProperty('state') == 'true' ? $this->setProperty($name,$value) :  $this->setProperty('errors',[$validate->getProperty('errors')]);

}
    public static function defData()
    {
        $statArr = [
            'table'=>'attributes',
            'dbfields'=>[
                'name', 'value'
            ],
            'types'=>[
                's', 's'
            ],
            'typesWhere'=>[
                's', 's','i'
            ],
        ];
        return $statArr;

    }
    public function insertProduct(){
        $staticDat =  $this->defData();
        $data =
            [
                $this->getProperty('name'),
                $this->getProperty('value'),
            ];
        $this->setProperty('data',$data);
        $conn = new Database();
        $conn->insert($staticDat['table'], $staticDat['dbfields'], $staticDat['types'],$data);
    }
    public static function getAllProducts(){
        $conn = new Database();
        $conn->selectTableData('product');
        return $conn->getProperty('result');
    }
    public static function findCategoriesProducts($categoriesName){
        $conn = new Database();
        $conn->whereQuery('product','*','category','=',$categoriesName);
        return $conn->getProperty('result');
    }
    public static function getByParam($param,$col=null){
        if ($col === null){
            $col = 'id';
        }
        $conn = new Database();
        $conn->whereQuery('product','*',$col,'=',$param);
        return $conn->getProperty('result');
    }
    public static function updateProduct($fields,$types,$data){
        $conn = new Database();
        //update fn in data first insert data then where param
        $conn->update('product', $fields, $types,'id' , '=', $data);
    }

    public static function deleteProduct($data){
        $conn = new Database();
        //update fn in data first insert data then where param
        $conn->deleteRow('product', $data);
    }


}