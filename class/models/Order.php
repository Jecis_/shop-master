<?php


namespace aptieka\models;

if (\session_status() == PHP_SESSION_NONE) {
    \session_start();
}


use aptieka\core\Model;
use aptieka\validation\Validate;

class Order extends Model implements DefModel
{
    public $order_data;
    public $user_id;
    protected $id;
    protected $types;
    protected $dbfields;
    public $errors;

    public function Validation()
    {
        $validate = new Validate();

        $validate->Validation($this->order_data, 'text');
        $validate->getProperty('state') == 'true' ? $this->order_data : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->user_id, 'text');
        $validate->getProperty('state') == 'true' ? $this->user_id : $this->setProperty('errors', [$validate->getProperty('errors')]);
    }

    public function __construct()
    {
        Order::init();
    }

    public static function init()
    {
        Model::$table = 'orders';
        Model::$select = '*';
        Model:: $action = '=';
        Model::$defdata = [
            'table' => 'orders',
            'dbfields' => [
                'order_data', 'user_id'
            ],
            'types' => [
                's', 'i'
            ],
            'typesWhere' => [
                's', 'i', 'i'
            ]
        ];
    }

    public static function Param($param)
    {
        Order::init();
        Model::$col = 'user_id';
        Model::$tableName = get_class();
        return Model::getByParam($param);
    }

    public function update($data)
    {
        Model::$col = 'id';
        parent::update($data);

    }
}


