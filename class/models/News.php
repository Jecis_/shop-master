<?php

namespace aptieka\models;

use aptieka\validation\Validate;
use aptieka\core\Model;

class News extends Model implements DefModel
{

    protected $text;
    protected $picture;
    protected $title;
    protected $id;
    protected $types;
    protected $dbfields;
    protected $categoryProducts;
    protected $getOne;
    public $errors;
    public function __construct()
    {
        News::init();
    }

    public static function init()
    {
        Model::$table = 'news';
        Model:: $action = '=';

        Model:: $defdata = [
            'table' => 'news',
            'dbfields' => [
                'text', 'picture', 'title'
            ],
            'types' => [
                's', 's', 's'
            ],
            'typesWhere' => [
                's', 's', 's', 'i'
            ]

        ];

    }

    public static function getAll()
    {
        News::init();
        return parent::getAll();
    }

    public static function byID($param)
    {

        Model::$col = 'id';
        Model::$select = '*';
        Model::$tableName = get_class();

        return Model::getByParam($param);
    }

    public function Validation()
    {
        $validate = new Validate();

        $validate->Validation($this->text, 'text');
        $validate->getProperty('state') == 'true' ? $this->text : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->picture, 'text');
        $validate->getProperty('state') == 'true' ? $this->picture : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->title, 'text');
        $validate->getProperty('state') == 'true' ? $this->title : $this->setProperty('errors', [$validate->getProperty('errors')]);
    }
}