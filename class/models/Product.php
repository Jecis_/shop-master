<?php

namespace aptieka\models;

use aptieka\database\Database;
use aptieka\validation\Validate;
use aptieka\core\Model;

class Product extends Model
{
    public $name;
    public $price;
    protected $discount;
    protected $category;
    protected $about;
    protected $prescription;
    protected $picture;
    protected $id;
    protected $allProducts;
    protected $categoryProducts;
    protected $types;
    protected $dbfields;
    public $count;


    public function __construct()
    {
        Product::init();
    }
    public static function getAll()
    {
        Product::init();
        return parent::getAll();
    }
    public static function Param($param)
    {
        Product::init();
        Model::$tableName = get_class();
        return Model::getByParam($param);
    }

    public static function init()
    {

        Model::$table = 'product';
        Model::$col = 'id';
        Model::$select = '*';
        Model:: $action = '=';
        Model:: $defdata = [
            'table' => 'product',
            'dbfields' => [
                'name', 'price', 'discount', 'category', 'about', 'prescription', 'picture'
            ],
            'types' => [
                's', 'd', 'i', 's', 's', 'i', 's'
            ],
            'typesWhere' => [
                's', 'd', 'i', 's', 's', 'i', 's', 'i'
            ],
        ];
    }

    public function Validation()
    {
        $validate = new Validate();

        $validate->Validation($this->name, 'text');
        $validate->getProperty('state') == 'true' ? $this->name : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->price, '/^[0-9]+(\.[0-9]{2})/');
        $validate->getProperty('state') == 'true' ? $this->price : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->discount, '/[0-9]/');
        $validate->getProperty('state') == 'true' ? $this->discount : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->category, 'text');
        $validate->getProperty('state') == 'true' ? $this->category : $this->setProperty('errors', [$validate->getProperty('errors')]);


        $validate->Validation($this->prescription, '/[01]{1}/');
        $validate->getProperty('state') == 'true' ? $this->prescription : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->about, 'text');
        $validate->getProperty('state') == 'true' ? $this->about : $this->setProperty('errors', [$validate->getProperty('errors')]);

        $validate->Validation($this->picture, 'text');
        $validate->getProperty('state') == 'true' ? $this->picture : $this->setProperty('errors', [$validate->getProperty('errors')]);

    }

}