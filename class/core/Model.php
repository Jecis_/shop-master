<?php
namespace aptieka\core;
use aptieka\validation\Validate;
use \aptieka\database\Database;
use mysql_xdevapi\Exception;

class Model
{
    protected static $table;
    protected static $defdata;
    protected static $select;
    protected static $column;
    protected static $action;
    protected static $col ;
    protected static $tableName;


    public function getProperty($name)
    {
        return $this->$name;
    }

    public  function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    public function insert($data){
        $staticDat =  Model::$defdata;

        $conn = new Database();
        $conn->insert($staticDat['table'], $staticDat['dbfields'], $staticDat['types'],$data);
    }
    public static function getAll(){
        $conn = new Database();
        $conn->selectTableData(Model::$table);
        return $conn->getProperty('result');
    }
    public static function find($data,$col=null){
        $conn = new Database();
        if(isset($col)){
            Model::$col = $col;
        }
        $conn->whereQuery(Model::$table,Model::$select,Model::$col,Model::$action,$data);
        return $conn->getProperty('result');
    }
    public static function getByParam($param){

        $conn = new Database();
        $conn->whereQueryObj(Model::$table,Model::$select,Model::$col,Model::$action,$param);
       $res =  $conn->getProperty('result');
        $classObj =  (new self)->mapObject($res);

        return $classObj;
    }
    public  function mapObject($res){
     if(!isset($res)){
         return false;
     }else{
         $class =Model::$tableName;
         $newclass = new $class();
         foreach($res as $property => $value) {
             $newclass->$property = $value;
         }
         return $newclass;
     }
    }

    public function update($data){
        $conn = new Database();
        $staticDat =  Model::$defdata;
        //update fn in data first insert data then where param

        try{
            $conn->update($staticDat['table'], $staticDat['dbfields'], $staticDat['typesWhere'],Model::$col , Model::$action, $data);
        }catch (\Error $exception){
            echo "error".$exception;
        }
    }


    public static function delete($data){
        $conn = new Database();
        //update fn in data first insert data then where param
        $conn->deleteRow(Model::$table, $data);
    }



}