<?php

namespace aptieka\auth;

use aptieka\Helpers;
use aptieka\models\User;

Class AuthActions extends User
{
    protected $errors;

    public function getProperty($name)
    {
        return $this->$name;
    }

    public function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    public function Register()
    {
        $user = new User();
        $user->name = htmlspecialchars($_REQUEST['name']);
        $user->surname = htmlspecialchars($_REQUEST['surname']);
        $user->email = htmlspecialchars($_REQUEST['email']);
        $user->address = htmlspecialchars($_REQUEST['address']);
        $user->personal_code = htmlspecialchars($_REQUEST['pers_code']);
        $user->password = $_REQUEST['password'];
        $user->is_active = '0';
        $user->Validation();
        if (isset($product->errors)) {
            session_destroy();
            $_SESSION['name'] = htmlspecialchars($_REQUEST['name']);
            $_SESSION['surname'] = htmlspecialchars($_REQUEST['surname']);
            $_SESSION['email'] = htmlspecialchars($_REQUEST['email']);
            $_SESSION['address'] = htmlspecialchars($_REQUEST['address']);
            $_SESSION['pers_code'] = htmlspecialchars($_REQUEST['pers_code']);
            return $this->errors = $user->errors;
        } else {
           $user->insert(Helpers::objToArr($user));
            $user_id = User::byEmail($user->email);

            Role::insertUserRoles($user_id->user_id, '2');
            header("Location: index.php");
            session_destroy();
            die();
        }
    }

    public function login()
    {

        $user = User::byEmail(htmlspecialchars($_REQUEST['email']));
        if (password_verify($_REQUEST['password'], $user->password)) {
            $authUser = PrivilegedUser::getByUsername(htmlspecialchars($_REQUEST['email']));
            $user->is_active = 1;
            $user->update(Helpers::objToArr($authUser));
            $_SESSION['activeUser'] = $_REQUEST['email'];
            header("Location: profile.php");
            die();
        } else {
            $this->setProperty('errors', 'Nepariezi ievadīts lietotājvārds/parole');
        }
    }

    public static function logout()
    {
//  connect to database
        $authUser = PrivilegedUser::getByUsername(htmlspecialchars($_SESSION['activeUser']));
        $authUser->is_active = 1;
        $authUser->update(Helpers::objToArr($authUser));
        session_destroy();
        header("Location: index.php");
        die();
    }

}