<?php

namespace aptieka\auth;
//require_once '../../autoload.php';

use aptieka\database\Database;

class Role
{
    protected $permissions;

    protected function __construct()
    {
        $this->permissions = array();
    }
    // return a role object with associated permissions
    public static function getRolePerms($role_id, $types)
    {
        $database = new Database();
//  connect to database
        $role = new Role();
        $sql = "SELECT t2.perm_desc FROM role_perm as t1
                JOIN permissions as t2 ON t1.perm_id = t2.perm_id
                WHERE t1.role_id = ?";
        $database->customQuery($sql, $role_id, $types);
        $res = $database->getProperty('result');
        $prem = [];
        foreach ($res as $key => $val) {
            $prem[] = $val['perm_desc'];
        }
        $role->permissions = $prem;
        //var_dump($res);
        return $role;
    }

    // check if a permission is set
    public function hasPerm($permission)
    {
       // var_dump($this->permissions);
        // $test =  array_key_exists($permission, $this->permissions);
        if ($test = array_search($permission, $this->permissions) !== false) {
            return 'nice';
        } else {
            return 'naughty';
        }
    }

    // insert a new role
    public static function insertRole($role_name)
    {
        $database = new Database();
//  connect to database
        $database->insert('roles', ['role_name'], ['s'], $role_name);
        return true;
    }

// insert array of roles for specified user id
    public static function insertUserRoles($user_id, $roles)
    {
        $database = new Database();
//  connect to database
        $database->insert('user_role', ['user_id', 'role_id'], ['i', 'i'], [$user_id, $roles]);
        return true;
    }

// delete array of roles, and all associations
    public static function deleteRoles($roles)
    {
        $database = new Database();
//  connect to database
        $connectionDB = $database->connectToDB();
        $role_id = ''; // input
        $sql = "DELETE t1, t2, t3 FROM roles as t1
            JOIN user_role as t2 on t1.role_id = t2.role_id
            JOIN role_perm as t3 on t1.role_id = t3.role_id
            WHERE t1.role_id = :role_id";
        $sth = $connectionDB->prepare($sql);
        $sth->bindParam(":role_id", $role_id, PDO::PARAM_INT);
        foreach ($roles as $role_id) {
            $sth->execute();
        }
        return true;
    }

// delete ALL roles for specified user id
    public static function deleteUserRoles($user_id)
    {
        $database = new Database();
//  connect to database
        $connectionDB = $database->connectToDB();
        $sql = "DELETE FROM user_role WHERE user_id = :user_id";
        $sth = $connectionDB->prepare($sql);
        return $sth->execute(array(":user_id" => $user_id));
    }
}

//$ROLE = Role::insertRole(['admin']);
//Role::insertUserRoles('19','1');
/*$rol = Role::getRolePerms('1', 'i')->hasPerm('allow');
if ($rol === 'nice') {
    echo 'it works';
}*/
//var_dump($rol);

