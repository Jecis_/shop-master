<?php

namespace aptieka\auth;
//require_once '../../autoload.php';

use aptieka\database\Database;
use aptieka\models\User;

class PrivilegedUser extends User
{
    private $roles;
    public $personal_code;
    public $name;
    public $email;
    public $surname;
    public $address;
    // override User method
    public static function getByUsername($email)
    {
      $result =  User::byEmail($email);
      //var_dump($result);
        if (!empty($result)) {
            $privUser = new PrivilegedUser();
            $privUser->user_id = $result->user_id;
            $privUser->name = $result->name;
            $privUser->surname = $result->surname;
            $privUser->address = $result->address;
            $privUser->personal_code = $result->personal_code;
            $privUser->password = $result->password;
            $privUser->email = $email;
            $privUser->initRoles();
            return $privUser;
        } else {
            return false;
        }
    }

    // populate roles with their associated permissions
    protected function initRoles()
    {
        $this->roles = array();
        $sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1
                JOIN roles as t2 ON t1.role_id = t2.role_id
                WHERE t1.user_id = ?";

        $database = new Database();

        $database->customQuery($sql, $this->user_id, 'i');

        $row = $database->getProperty('result');
if($row !== null){
    foreach ($row as $r) {
        $this->roles[$r["role_name"]] = Role::getRolePerms($r["role_id"], 'i');
    }
}


    }

    public static function insertPermission($perm)
    {
        $database = new Database();
         $database->insert('permissions', ['perm_desc'], ['s'], $perm);

    }

    // check if user has a specific privilege
    public function hasPrivilege($perm)
    {
        foreach ($this->roles as $role) {
            //var_dump();
            if ($role->hasPerm($perm) === 'nice') {
                return true;
            }
        }
        return false;
    }

    // check if a user has a specific role
    public function hasRole($role_name)
    {
        return isset($this->roles[$role_name]);
    }

// insert a new role permission association
    public static function insertPerm($role_id, $perm_id)
    {
        $database = new Database();
//  connect to database
        $database->insert('role_perm', ['role_id', 'perm_id'], ['i', 'i'], [$role_id, $perm_id]);

    }

// delete ALL role permissions
    public static function deletePerms()
    {
        $database = new Database();
//  connect to database
        $database->drop('role_perm');
        return;
    }
}
//$test = PrivilegedUser::getByUsername('adzzzzzmin@admin.com');

//$test->hasPrivilege('de');
/*var_export($test->hasRole('admin'));
var_dump($test->hasPrivilege('dope'));*/