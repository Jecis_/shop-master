<?php
namespace aptieka\validation;
class Validate{
   protected $state;
   protected $errors = [];
    public function getProperty($name)
    {
        return $this->$name;
    }
    public function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    public function Validation($string,$pattern){

        if($string == ''){
            $this->setProperty('state','false') ;
            return $this->setProperty('errors',['Empty input :'.$string.$pattern]);
        }else{
            if($pattern === 'text'){
                return $this->setProperty('state','true');
            }
            if($pattern === 'email'){
                if(!isset($_REQUEST['email']) ){
                    $str =  filter_var($string, FILTER_VALIDATE_EMAIL);
                    if($str =='false'){
                        $this->setProperty('state','false') ;
                        return $this->setProperty('errors',['Kļūda ievadītajos datos'.$string]);
                    }else{
                        return $this->setProperty('state','true');
                    }
                }else
                $database = new \aptieka\database\Database();
                $database->whereQuery('users','email','email','=',$_REQUEST['email']);
                $res = $database->getProperty('result');
               if(isset($res) && count($res)>0){
                   $this->setProperty('state','false') ;
                   return $this->setProperty('errors',['Epasta adrese jau tiek izmantota'.$string]);
               }else{
                   $str =  filter_var($string, FILTER_VALIDATE_EMAIL);
                   if($str =='false'){
                       $this->setProperty('state','false') ;
                       return $this->setProperty('errors',['Kļūda ievadītajos datos'.$string]);
                   }else{
                       return $this->setProperty('state','true');
                   }
               }

            }else{
                if (!preg_match($pattern, $string)) {
                    $this->setProperty('state','false') ;
                    return $this->setProperty('errors',['Kļūda ievadītajos datos :'.$string]);
                } else {
                    return $this->setProperty('state','true');
                }
            }
        }
    }
}