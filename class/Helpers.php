<?php
/**
 * Created by PhpStorm.
 * User: jecis
 * Date: 3/8/2019
 * Time: 11:27
 */
namespace aptieka;
class Helpers
{
    public static function objToArr($obj)
    {
        $obj = (array)$obj;
        $obj = array_values($obj);
        foreach ($obj as $key => $value){
            if($obj[$key] === null ){
                unset($obj[$key]);
            }
        }
        return $obj;
    }
}