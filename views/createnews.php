<?php
$errors;
\session_start();
require_once '../autoload.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $news = New \aptieka\controllers\NewsController();
    $news->addNews();
    //$errors= $product->getProperty('errors');
    /* if(isset($errors)){
        echo $errors;
     }*/
}
use aptieka\auth\PrivilegedUser;
if (isset($_SESSION['activeUser'])) {
    $database = new \aptieka\database\Database();
    $authUser = PrivilegedUser::getByUsername(htmlspecialchars($_SESSION['activeUser']));
    $ac = 1;
} else $ac = 0;

?>
<head>
    <link rel="stylesheet" href="../css/productadd.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<!--<form action="
                <?php
/*echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
*/ ?>"
      method="post"
>-->
<div id="container">

    <div class="p-2 d-flex" id="header">
        <p class="ml-2 mb-0 text-light">RAKSTU PIEVIENOŠANA</p>

        <div>
            <a class="btn btn-outline-light ml-sm-1" href="../index.php" role="button">Sākums</a>
            <?php if ($authUser->hasRole('admin') || $authUser->hasRole('productManager')) {
                ?>
                <a class="btn btn-outline-light ml-sm-1" href="createproduct.php" role="button">Pievienot produktu</a>
                <a class="btn btn-outline-light ml-sm-1" href="visiprodukti.php" role="button">Visi produkti</a>
            <?php }
            if ($authUser->hasRole('admin') || $authUser->hasRole('editor')) {
                ?>
                <a class="btn btn-outline-light ml-sm-1" href="createnews.php" role="button">Pievienot rakstu</a>
                <a class="btn btn-outline-light ml-sm-1" href="visiraksti.php" role="button">Visi raksti</a>
            <?php } ?>
        </div>
    </div>

    <div id="main" class="p-4 d-flex">

        <form class="pt-sm-4" action="
                <?php
        echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
        ?>"
              method="post">

            <ul class="nav nav-tabs col-lg-12 mb-sm-3" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#general" role="tab"
                       aria-controls="home" aria-selected="true">Raksts</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <input type="hidden" class="form-control" name="forma">
                <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="home-tab">
                    <div class="form-group row mb-sm-2 mb-lg-3">
                        <label for="cName" class="col-lg-3 col-form-label">Nosaukums</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="cName" name="title">
                        </div>
                    </div>
                    <div class="form-group row mb-sm-2 mb-lg-3">
                        <label for="cTitle" class="col-lg-3 col-form-label">Teksts</label>
                        <div class="col-lg-9">
                            <textarea rows="15" cols="50" type="text" class="form-control" id="cTitle"
                                      name="text"></textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-sm-2 mb-lg-3">
                        <label for="cParent" class="col-lg-3 col-form-label">Attēls</label>
                        <div class="input-group col-lg-9">
                            <input type="text" id="cParent" class="form-control"
                                   aria-label="Text input with segmented dropdown button" name="picture">
                        </div>
                    </div>

                </div>

            </div>
            <button class="btn btn-success" value="submit" name="submit">Saglabāt</button>
        </form>
    </div>
</div>
<!--</form>-->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
