<?php
$errors;
\session_start();
require_once '../autoload.php';
$allProducts = \aptieka\models\Product::getAll();
//var_dump($allProducts);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $product = New \aptieka\controllers\ProductController();
    if ($_REQUEST["action"] == "delete") {
        $product->deleteProduct();
    } elseif ($_REQUEST["action"] == "update") {
        $product->updateProduct();

    }
    header("Location: visiprodukti.php");
    //$errors= $product->getProperty('errors');
    /* if(isset($errors)){
        echo $errors;
     }*/
}

use aptieka\auth\PrivilegedUser;

if (isset($_SESSION['activeUser'])) {
    $database = new \aptieka\database\Database();
    $authUser = PrivilegedUser::getByUsername(htmlspecialchars($_SESSION['activeUser']));
    $ac = 1;
} else $ac = 0;

?>
<head>
    <link rel="stylesheet" href="../css/productadd.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style>
        .pt-3-half {
            padding-top: 1.4rem;
        }
    </style>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<div id="container-fluid">

    <div class="p-2 d-flex" id="header">
        <p class="ml-2 mb-0 text-light">VISI PRODUKTI</p>

        <div>
            <a class="btn btn-outline-light ml-sm-1" href="../index.php" role="button">Sākums</a>
            <?php if ($authUser->hasRole('admin') || $authUser->hasRole('productManager')) {
                ?>
                <a class="btn btn-outline-light ml-sm-1" href="createproduct.php" role="button">Pievienot produktu</a>
                <a class="btn btn-outline-light ml-sm-1" href="visiprodukti.php" role="button">Visi produkti</a>
            <?php }
            if ($authUser->hasRole('admin') || $authUser->hasRole('editor')) {
                ?>
                <a class="btn btn-outline-light ml-sm-1" href="createnews.php" role="button">Pievienot rakstu</a>
                <a class="btn btn-outline-light ml-sm-1" href="visiraksti.php" role="button">Visi raksti</a>
            <?php } ?>
        </div>
    </div>

    <div id="main" class="p-4 d-flex">
        <div class="row">
            <div class="card col-lg-12">
                <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Visi produkti</h3>

                <div class="card-body">
                    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i class="fa fa-plus fa-2x"
                                                                                         aria-hidden="true"></i></a></span>
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <tr>
                                <th class="text-center">Nosaukums</th>
                                <th class="text-center">Cena</th>
                                <th class="text-center">Atlaide</th>
                                <th class="text-center">Kategorija</th>
                                <th class="text-center">Apraksts</th>
                                <th class="text-center">Recepšu/bezrecepšu</th>
                                <th class="text-center">Attēls</th>
                                <th class="text-center">Rediģēt</th>
                                <th class="text-center">Dzēst</th>
                            </tr>
                            <?php foreach ($allProducts as $key => $product) {
                                ?>
                                <tr>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['name'] ?></td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['price'] ?></td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['discount'] ?> %
                                    </td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['category'] ?></td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['about'] ?></td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['prescription'] ?></td>
                                    <td class="pt-3-half"
                                        contenteditable="false"><?php echo $allProducts[$key]['picture'] ?></td>
                                    <td>
                                    <span><button type="button"
                                                  class="btn btn-primary btn-rounded btn-sm my-0" data-toggle="modal"
                                                  data-target="#myModal<?php echo $key
                                                  ?>">Rediģēt</button></span>
                                    </td>
                                    <td>
                                        <form action="
                <?php
                                        echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                                        ?>"
                                              method="post">
                                            <input type="hidden" id="cParent" class="form-control"
                                                   aria-label="Text input with segmented dropdown button"
                                                   value="<?php echo $allProducts[$key]['id'] ?>" name="id">
                                            <input type="hidden" id="cParent" class="form-control"
                                                   aria-label="Text input with segmented dropdown button" value="delete"
                                                   name="action">

                                            <button class="btn btn-danger mx-auto btn-rounded btn-sm my-0" value="submit" name="submit">Dzēst</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($allProducts as $key => $product) { ?>
    <div class="modal fade" id="myModal<?php echo $key ?>">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">REDIĢĒT</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">

                            <form class="pt-sm-4" action="
                <?php
                            echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                            ?>"
                                  method="post">

                                <div class="tab-content" id="myTabContent">
                                    <input type="hidden" class="form-control" name="forma">
                                    <div class="tab-pane fade show active" id="general" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cName" class="col-lg-3 col-form-label">Nosaukums</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="cName" name="name"
                                                       value="<?php echo $allProducts[$key]['name'] ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cTitle" class="col-lg-3 col-form-label">Cena</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="cTitle" name="price"
                                                       value="<?php echo $allProducts[$key]['price'] ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cParent" class="col-lg-3 col-form-label">Atlaide</label>
                                            <div class="input-group col-lg-9">
                                                <input type="text" id="cParent" class="form-control"
                                                       aria-label="Text input with segmented dropdown button"
                                                       value="<?php echo $allProducts[$key]['discount'] ?>"
                                                       name="discount" required>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cParent" class="col-lg-3 col-form-label">Kategorija</label>
                                            <div class="input-group col-lg-9">
                                                <input type="text" id="cParent" class="form-control"
                                                       aria-label="Text input with segmented dropdown button"
                                                       value="<?php echo $allProducts[$key]['category'] ?>"
                                                       name="category" required>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cParent" class="col-lg-3 col-form-label">Apraksts</label>
                                            <div class="input-group col-lg-9">
                                                    <textarea rows="3" cols="50" type="text" id="cParent"
                                                              class="form-control"
                                                              aria-label="Text input with segmented dropdown button"
                                                              name="about"
                                                              required><?php echo $allProducts[$key]['about'] ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cParent" class="col-lg-3 col-form-label">Recepšu (0/1)</label>
                                            <div class="input-group col-lg-9">
                                                <input type="text" id="cParent" class="form-control"
                                                       aria-label="Text input with segmented dropdown button"
                                                       value="<?php echo $allProducts[$key]['prescription'] ?>"
                                                       name="prescription" required>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-sm-2 mb-lg-3">
                                            <label for="cParent" class="col-lg-3 col-form-label">Attēls</label>
                                            <div class="input-group col-lg-9">
                                                    <textarea rows="3" cols="50" type="text" id="cParent"
                                                              class="form-control"
                                                              aria-label="Text input with segmented dropdown button"
                                                              name="picture"
                                                              required><?php echo $allProducts[$key]['picture'] ?></textarea>
                                            </div>
                                        </div>

                                        <input type="hidden" id="cParent" class="form-control"
                                               aria-label="Text input with segmented dropdown button"
                                               value="<?php echo $allProducts[$key]['id'] ?>" name="id">
                                        <input type="hidden" id="cParent" class="form-control"
                                               aria-label="Text input with segmented dropdown button" value="update"
                                               name="action">

                                    </div>

                                </div>
                                <button class="btn btn-success mx-auto" value="submit" name="submit">Saglabāt</button>
                            </form>

                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mx-auto" data-dismiss="modal">AIZVĒRT</button>
                </div>

            </div>
        </div>
    </div>
<?php } ?>
<!--</form>-->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
