<?php
$errors=array();
\session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once '../autoload.php';
    $reg = New \aptieka\auth\AuthActions();
    $reg->Register();
    $errors= $reg->getProperty('errors');
    if(isset($errors)&& !empty($errors)){
        foreach ($errors[0] as $key =>$value){
            echo $value;
        }
    }
}
?>
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../css/register.css">
</head>

<div class="container register">
    <div class="row">
        <div class="col-md-3 register-left">
            <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
            <h3>Welcome</h3>
            <p>You are 30 seconds away from earning your own money!</p>
            <input type="submit" name="" value="Login"/><br/>
        </div>
        <div class="col-md-9 register-right">
            <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                method="post"
            >
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <h3 class="register-heading">Apply as a Employee</h3>
                    <div class="row register-form">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Vārds *" value="<?php echo isset($_SESSION['name'])?($_SESSION['name'] !=''? ( $_SESSION['name']):'') :'' ?>"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="surname" class="form-control" placeholder="Uzvārds *" value="<?php echo isset($_SESSION['surname'])?($_SESSION['surname'] !=''? $_SESSION['surname']:''):'' ?>"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="address" class="form-control" placeholder="Adrese *" value="<?php echo isset($_SESSION['address'] )?($_SESSION['address'] !=''? $_SESSION['address']:''):''  ?>"/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="pers_code" class="form-control" placeholder="Personas kods *" value="<?php echo isset($_SESSION['pers_code'])?($_SESSION['pers_code'] !=''? $_SESSION['pers_code']:''):''  ?>"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="E-pasts *" value="<?php echo isset($_SESSION['email'])?($_SESSION['email'] !=''? $_SESSION['email']:''):''  ?>"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Parole *" value=""/>
                            </div>
                            <input type="submit" class="btnRegister" value="Register"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>