<?php
require_once 'autoload.php';

use aptieka\auth\PrivilegedUser;

\session_start();
if (isset($_SESSION['activeUser'])) {

    $authUser = PrivilegedUser::getByUsername(htmlspecialchars($_SESSION['activeUser']));


    $_SESSION['user'] = $authUser->user_id;
    //var_dump($authUser->user_id);
    $ac = 1;



} else $ac = 0;


?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/landing-page.css" rel="stylesheet">
</head>

<div id="app" class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php"><img src="img/Francesca%20M%20(2).png"
                                                      style="width:200px; height: 50px;"></a>
        <ul class="navbar-nav ml-auto">
            <?php
            if ($ac == 1) {
                if ($authUser->hasRole('admin') || $authUser->hasRole('productManager')) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="views/createproduct.php">Pievienot produktu</a>
                    </li>
                <?php }
                if ($authUser->hasRole('admin') || $authUser->hasRole('editor')) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="views/createnews.php">Pievienot rakstu</a>
                    </li>
                <?php }
            } ?>
            <li class="nav-item <?php if ($currentPage === 'index') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="index.php">Sākums</a>
            </li>
            <li class="nav-item <?php if ($currentPage === 'aktualitates') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="aktualitates.php">Aktualitātes</a>
            </li>
            <li class="nav-item <?php if ($currentPage === 'produkti') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="produkti.php">Produkti</a>
            </li>
            <li class="nav-item <?php if ($currentPage === 'ieteikumi') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="ieteikumi.php">Ieteikumi</a>
            </li>
            <li class="nav-item <?php if ($currentPage === 'kontakti') {
                echo 'active';
            } ?>">
                <a class="nav-link" href="kontakti.php">Kontakti</a>
            </li>
            <?php if ($ac == 0) { ?>
                <li class="nav-item <?php if ($currentPage === 'login') {
                    echo 'active';
                } ?>">
                    <a class="nav-link" href="loginpage.php">Ieiet</a>
                </li>
                <li class="nav-item <?php if ($currentPage === 'register') {
                    echo 'active';
                } ?>">
                    <a class="nav-link" href="registerpage.php">Reģistrēties</a>
                </li>
            <?php } ?>
            <?php
            if ($ac == 1) {
                ?>
                <li class="nav-item <?php if ($currentPage === 'profils') {
                    echo 'active';
                } ?>">
                    <a class="nav-link" name="submit" href="profile.php">Profils</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" name="submit" href="logout.php">Iziet</a>
                </li>
                <li class="nav-item">
                    <div id="cart-container">
                        <div id="cart" class="btn btn-success" data-toggle="modal" data-target="#shoppingcart">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                        </div>
                        <span id="itemCount"></span>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </nav>
</div>