<!DOCTYPE html>
<html lang="en">

<?php $title = 'Aktualitātes'; ?>
<?php $currentPage = 'aktualitates'; ?>
<?php include('navbar.php'); ?>
<?php
$news = \aptieka\models\News::getAll();
$count = count($news);
?>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $product = New \aptieka\controllers\ActionController();
    $product->addSubs();
}
?>
<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Aktualitātes</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="showcase">
    <div class="container-fluid p-0">
        <?php foreach ($news as $key => $value){ ?>
        <?php if ($key%2==1) { ?>
        <div class="row uno-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('<?php echo $news[$key]['picture']   ?>');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2><?php echo $news[$key]['title']   ?></h2>
                <p class="lead mb-0"><?php echo $news[$key]['text']   ?>
                </p>
            </div>
        </div>
            <?php if ($key!=$count-1) { ?>
                <section class="innersection2 text-white text-center">
                    <div class="overlay"></div>
                </section>
            <?php }?>
            <?php } else {?>
                <div class="row uno-gutters">
                    <div class="col-lg-6 text-white showcase-img"
                         style="background-image: url('<?php echo $news[$key]['picture']   ?>');"></div>
                    <div class="col-lg-6 my-auto showcase-text">
                        <h2><?php echo $news[$key]['title']   ?></h2>
                        <p class="lead mb-0"><?php echo $news[$key]['text']   ?></p>
                    </div>
                </div>
                <?php if ($key!=$count-1) { ?>
                    <section class="innersection2 text-white text-center">
                        <div class="overlay"></div>
                    </section>
                <?php }?>
        <?php }?>
        <?php }?>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action2 text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                      method="post">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg" placeholder="Lūdzu, ievadi e-pasta adresi..." required name="email">
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
