<!DOCTYPE html>
<html lang="en">

<head>

    <?php $title = 'Kontakti'; ?>
    <?php $currentPage = 'kontakti'; ?>
    <?php include('navbar.php'); ?>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $product = New \aptieka\controllers\ActionController();
        $product->addSubs();
    }
    ?>
</head>

<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Kontakti</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
    <div class="container-fluid">
        <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Mūsu adrese:</h4>
                    <p class="card-text">Brīvības iela 26, Rīga</p>
                    <h4 class="card-title">Kontakttālrunis:</h4>
                    <p class="card-text">20368224</p>
                    <h4 class="card-title">E-pasts:</h4>
                    <p class="card-text">info@dabasaptieka.lv</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
        </div>
        </div>
        <section class="call-to-action1 text-white text-center mt-3">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <h2>Aptieku tīkls</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="row mt-3">
        <div class="col-lg-12">
        <iframe width="100%" height="600px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                src="https://www.google.com/maps/d/u/0/embed?mid=1zjJj6Cf8kFaJ_POBqjMSJIVoviT5wnPm&amp;z=8" width="100%" height="600"></iframe>
        <br/>
        <small>
            <a href="https://www.google.com/maps/d/u/0/embed?mid=1zjJj6Cf8kFaJ_POBqjMSJIVoviT5wnPm&amp;z=8" width="100%" height="600"></a>
        </small>
        </div>
    </div>
    </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                      method="post">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg"
                                   placeholder="Lūdzu, ievadi e-pasta adresi..." required name="email">
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
