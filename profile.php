<!DOCTYPE html>
<html lang="en">
<?php $title = 'Profils'; ?>
<?php $currentPage = 'profils'; ?>
<?php include('navbar.php'); ?>
<?php
require_once 'autoload.php';

use aptieka\auth\PrivilegedUser;
$authUser = PrivilegedUser::getByUsername(htmlspecialchars($_SESSION['activeUser']));

?>

<body>

<!-- Masthead -->
<header class="masthead1 text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Mans profils</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
    <div class="container-fluid">
        <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Profila informācija:</h4>
                    <p class="card-text">Vārds, uzvārds: <?php echo $authUser->name .' ' .$authUser->surname?></p>
                    <p class="card-title">E-pasts: <?php echo $authUser->email?></p>
                    <p class="card-text">Adrese: <?php echo $authUser->address?></p>
                    <p class="card-title">Personas kods: <?php echo $authUser->personal_code?></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
        </div>
        </div>
    </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
