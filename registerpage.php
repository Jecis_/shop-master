<!DOCTYPE html>
<html lang="en">

<head>

    <?php $title = 'Reģistrēties'; ?>
    <?php $currentPage = 'register'; ?>
    <?php include('navbar.php'); ?>

    <?php
    $errors;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        require_once 'autoload.php';
        $reg = New \aptieka\auth\AuthActions();
        $reg->Register();
        $errors = $reg->getProperty('errors');
        if (isset($errors)) {
            echo $errors;
        }
    }
    ?>

</head>

<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Reģistrācija</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Reģistrēties:</h4>
                        <form action="
                <?php
                        echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                        ?>"
                              method="post"
                              class="login-form">
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="">Vārds:</label>
                                <input type="text" name="name" class="form-control" placeholder="">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="">Uzvārds:</label>
                                <input type="text" name="surname" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="">E-pasts:</label>
                                <input type="text" name="email" class="form-control" placeholder="">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="">Adrese:</label>
                                <input type="text" name="address" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="">Personas kods:</label>
                                <input type="text" name="pers_code" class="form-control" placeholder="">

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="">Parole:</label>
                                <input type="password" name="password" class="form-control" placeholder="">
                            </div>
                            <div class="form-check">
                                <button type="submit" class="btn btn-login float-right">Reģistrēties</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            </div>
        </div>
    </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form>
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg"
                                   placeholder="Lūdzu, ievadi e-pasta adresi..." required>
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
