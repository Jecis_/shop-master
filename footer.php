<!-- Footer -->

<footer class="footer bg-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 h-100 text-center my-auto">
                <ul class="list-inline mb-2">
                    <li class="list-inline-item <?php if ($currentPage === 'aktualitates') {
                        echo 'active';
                    } ?>">
                        <a href="aktualitates.php" class="text-success">Aktualitātes</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item <?php if ($currentPage === 'produkti') {
                        echo 'active';
                    } ?>">
                        <a href="produkti.php" class="text-success">Produktu klāsts</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item <?php if ($currentPage === 'ieteikumi') {
                        echo 'active';
                    } ?>">
                        <a href="ieteikumi.php" class="text-success">Ekspertu ieteikumi</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item <?php if ($currentPage === 'kontakti') {
                        echo 'active';
                    } ?>">
                        <a href="kontakti.php" class="text-success">Kontakti</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" id="shoppingcart">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">GROZS</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" >
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                PASŪTĪJUMS

                                    <div class="container-fluid">
                                        <div class="row">
                                            <?php
                                            $order = new \aptieka\controllers\OrderController();
                                            $products =   $order->cart();
                                            ?>
                                            <?php foreach ($products as $key => $val) {

                                                ?>
                                                <div class="col-sm-12">
                                                    <div class="card s-2" style="height: auto;">
                                                        <div class="card-body">
                                                            <h4 class="card-title"><?php echo $val['name'] ?></h4>
                                                            <p class="card-text"><?php echo $val['price'] ?></p>
                                                            <p class="card-text">Daudzums<?php echo $val['count'] ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }  ?>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mx-auto" data-dismiss="modal">AIZVĒRT</button>
            </div>

        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
    var itemCount = 0;

    $('.add').click(function () {
        itemCount++;
        $('#itemCount').html(itemCount).css('display', 'block');
        $.ajax({
            method: "POST",
            url: '/class/controllers/ajaxController.php',
            data: {
                action: $(this).siblings('.count').text(),
                user_id: $(this).siblings('.user').text(),
            },
            success: function (data) {
                console.log(data);
            }
        });
        console.error('lol');

    });

    $('.clear').click(function () {
        itemCount = 0;
        $('#itemCount').html('').css('display', 'none');
        $('#cartItems').html('');
    });
</script>