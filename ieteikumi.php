<!DOCTYPE html>
<html lang="en">

<?php $title = 'Ieteikumi'; ?>
<?php $currentPage = 'ieteikumi'; ?>
<?php include('navbar.php'); ?>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $product = New \aptieka\controllers\ActionController();
    $product->addSubs();
}
?>
<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Ekspertu ieteikumi</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="showcase">
    <div class="container-fluid p-0">
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-533360.jpeg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>VITAMĪNI, KAS UZLABO IMŪNSISTĒMU</h2>
                <p class="lead mb-0">Šajā periodā ir svarīgi sevi pasargāt no baciļiem, saglabājot stipru imūnsistēmu,
                    lai nezaudētu efektivitāti savos ikdienas darbos, saglabātu labu pašsajūtu un harmoniju ar sevi! Par
                    to, kādi vitamīni nepieciešami tavam organismam drūmajā saaukstēšanās un baciļu laikā, uzzini lasot
                    zemāk.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/shutterstock_522552706.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>1. A Vitamīns</h2>
                <p class="lead mb-0">A vitamīns ne tikai nodrošina gludu un skaistu ādu, bet ir arī ļoti svarīgs
                    imunitātei un gļotādas veselībai, it sevišķi laikā, kad baciļi lido apkārt un slimo katrs otrais
                    draugs un kolēģis. Tas samazina iespēju saslimt ar infekcijslimībām, vīrusiem, baktērijām un
                    slimības gadījumā nomāc simptomus. Rūpēs par savu veselību burkāni, mango, spināti un aprikozes būs
                    perfektās sastāvdaļas tavam ik rīta smūtijam. Tāpat vitamīnu A, vari iegūt arī no piena, čederas
                    siera, tomātiem, ķirbja, brokoļiem vai aknām.</p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/shutterstock_461506093.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>2. Vitamīns C</h2>
                <p class="lead mb-0">Vitamīns C jau izsenis zināms, kā lielisks imunitātes stiprinātājs, kas palīdz tikt
                    galā ar saaukstēšanos, iesnām un dažādām citām vīrusu infekcijām. Ja, tomēr esi noķēris kādu negantu
                    vīrusu, vitamīnam C būs svarīga loma iespējamo komplikāciju risku – plaušu infekciju un pneimonijas,
                    mazināšanā. Vitamīnu vari uzņemt pārtikā lietojot tādus augļus un dārzeņus, kā sarkano un zaļo
                    papriku, kivi, upenes, zemenes, apelsīnus un brokoļus.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/shutterstock_527366725.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>3. Cinks</h2>
                <p class="lead mb-0">Stipra asinsrite un organisma dabīgās aizsargspējas? To ir iespējams iegūt uzņemot
                    nepieciešamo daudzumu Cinka, kas palīdzēs izvairīties no šņaukāšanās un šķavām rudens mainīgajos
                    laikapstākļos. Šo minerālvielu iespējams iegūt no dažādiem uztura bagātinātājiem, kā arī no pārtikas
                    produktiem – krabjiem, pupiņām, sarkanās gaļas, kefīra, turku zirņiem un ķirbju sēklām. Tikai
                    atceries, ka cinku ieteicam uzņemt aptuveni 8 miligramus dienā, pretējā gadījumā tas nevis uzlabos,
                    bet pazeminās imunitāti.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/shutterstock_470347046.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>4. Vitamīns D</h2>
                <p class="lead mb-0">Gada tumšajos mēnešos mūsu veselībai jo īpaši ir nepieciešams D vitamīns. Tas
                    atbalsta un regulē imūnsistēmu, uzlabo pašsajūtu, kā arī palīdz uzņemt fosforu un kalciju organismā.
                    Tāpēc, kad saulīte ārā mūs vairs nelutina ar nepieciešamo šī vērtīgā vitamīna devu, ir svarīgi
                    atrast citus veidus, kā to uzņemt. Viens no D vitamīna uzņemšanas avotiem ir uztura bagātinātāji,
                    taču to ir iespējams uzņemt arī ar tādiem pārtikas produktiem kā – zivju eļļa, olas, lasis, tuncis,
                    jogurts un siers.
                </p>
            </div>
        </div>
        <section class="innersection2 text-white text-center">
            <div class="overlay"></div>
        </section>
        <div class="row uno-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('img/scent-sticks-fragrance-aromatic-161599.jpeg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <h2>AROMTERAPIJA IKDIENĀ – KURAS SMARŽAS UZMUNDRINA, BET KURAS NOMIERINA?</h2>
                <p class="lead mb-0">Ēterisko eļļu dziedinošais spēks un ietekme uz cilvēka veselību ir zināma jau
                    gadsimtiem ilgi. Tās tika izmantotas senajās kultūrās Indijā, Ēģiptē, Ķīnā un Dienvideiropā.
                    Mūsdienās, izvēloties aizvien dabiskākus līdzekļus veselības uzlabošanai, ēteriskās eļļas piedzīvo
                    jaunu popularitātes vilni.</p>
            </div>
        </div>
        <div class="row uno-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-531539.jpeg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <p class="lead mb-0">Aromterapijas pamatā ir ēterisko eļļu izmantošana, lai līdzsvarotu un dziedinātu
                    cilvēka fizisko un garīgo ķermeni. Kopumā pasaulē ir vairāk nekā 200 dažādas ēteriskās eļļas un
                    katrai no tām piemīt dažādas spējas, kas var sniegt enerģiju, relaksāciju, veselību un labsajūtu.
                    Pareizi lietotas tās var pozitīvi ietekmēt garastāvokli un arī fizisko veselību. Lai iegūtu
                    vislabāko efektu, vienu un to pašu ēterisko eļļu nav ieteicams lietot ilgāk par 10 dienām pēc
                    kārtas.
                </p>
                <p class="lead mb-0">Vispirms būtu jāiegaumē, ka ēteriskās eļļas netiek lietotas iekšķīgi. Tās cilvēka
                    ķermenī nonāk caur masāžu, inhalāciju, ierīvēšanu, aromātiskām peldēm vai kompresēm, kā arī
                    izmantojot kosmētiskos līdzekļus, kuru sastāvā ir ēteriskās eļļas.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-129574.jpeg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <p class="lead mb-0">Ja viena no pirmajām domām pamostoties ir „kaut vēl dažas stundiņas varētu
                    pagulēt”, nogurums un bezspēks ir tev pazīstams. Tā vietā, lai paļautos uz rīta kafijas krūzi,
                    izmēģini ēteriskās eļļas, kas palīdzēs justies svaigai, enerģiskai un paaugstinās produktivitāti.
                    Citrusaugļu eļļas, piemēram, apelsīnu, citronu un greipfrūtu eļļa, vienmēr ir bijušas lielisks
                    tūlītējas enerģijas lādiņš. Svaigie citrusaugļu aromāti lieliski palīdz pret depresiju un stresu,
                    kas var būt arī noguruma un nespēka cēloņi.
                </p>
                <p class="lead mb-0">Tāpat tūlītēju enerģiju saņemsi izmantojot piparmētru un krūzmētru eļļu. Bazilika
                    eļļa palīdzēs stimulēt virsnieru dziedzerus, kas ir nozīmīgi garīgā noguruma samazināšanā.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-416457.jpeg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <p class="lead mb-0">Cīņā pret
                    stresu un satraukumu izmanto rozmarīna eļļu, kas palīdzēs uzlabot garastāvokli un enerģijas
                    daudzumu. Ja radušās problēmas iemigt, izmanto nomierinošo lavandas eļļu. Ne velti
                    šī eļļa tiek uzskatīta par visnomierinošāko un ieguvusi milzu popularitāti. Tā ne vien palīdz
                    atbrīvoties, bet arī mazina galvassāpes. Cīņā ar bezmiegu izmanto arī vaniļas, anīsa un ilang-ilang
                    eļļu, kas radīs līdzsvaru, harmoniju un eiforiskas sajūtas.
                </p>
                <p class="lead mb-0">Relaksējošam efektam izmanto kumelīšu eļļu, kas palīdz mazināt trauksmi, panikas
                    sajūtu un ļaus atbrīvoties no aizvainojuma un dusmām. Nomierinošas īpašības piemīt arī ciedru koka
                    eļļai. Tās siltais meža aromāts iedarbojas uz dziedzeri, kas atbildīgs par miega hormona –
                    melatonīna, izdalīšanos organismā.
                </p>
            </div>
        </div>
        <section class="innersection2 text-white text-center">
            <div class="overlay"></div>
        </section>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-273238.jpeg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>PAVINGRO BIROJĀ!</h2>
                <p class="lead mb-0">Mūsdienu straujajā dzīves ritmā dažkārt neatliek laika sportiskām aktivitātēm, lai
                    gan tās ir ļoti nepieciešamas, lai dzīvotu veselīgi. Ja tu birojā pavadi astoņas stundas jeb
                    trešdaļu no diennakts, kāpēc turpat arī nepavingrot?
                </p>
                <p class="lead mb-0">Daudzos birojos tavas sportiskās aktivitātes kopējo telpu vidū varētu nesaprast.
                    Tāpēc esam apkopojuši vairākus vienkāršus vingrojumus, ko vari nemanāmi veikt pie sava darba galda!
                </p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-167704.jpeg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Kakls</h2>
                <p class="lead mb-0">Treniņu birojā sāc ar vieglu vingrinājumu kaklam! Lēni noliec galvu uz vienu pusi,
                    tuvinot to plecam. Brīdi to tā paturi un atkārto, noliecot galvu pie otra pleca.
                </p>
                <br>
                <h2>Pleci</h2>
                <p class="lead mb-0">Pēc kakla izvingrināšanas jāatbrīvo pleci, kas biroja darbiniekiem nereti ir
                    saspringti. Desmit reizes apļo plecus virzienā uz priekšu. Pēc tam atkārto vingrinājumu, apļojot
                    plecus pretējā virzienā.
                </p>
            </div>
        </div>
        <div class="row uno-gutters">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/pexels-photo-612892.jpeg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Plaukstas locītavas</h2>
                <p class="lead mb-0">Sagatavojies darbam pie datora, izvingrinot plaukstas locītavas. Izstiep uz priekšu
                    vienu roku. Ar otru roku pieturi uz āru izvērsto plaukstu ar pirkstiem uz leju. Brīdi paliec tādā
                    pašā pozā, tad pagriez pirkstus uz augšu un atkārto vingrinājumu. Atkārto trīs reizes ar katru roku.
                </p>
            </div>
        </div>
        <div class="row uno-gutters1">

            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('img/legs-window-car-dirt-road-51397.jpeg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2>Kājas</h2>
                <p class="lead mb-0">Ilga sēdēšana nenāk par labu taviem kāju muskuļiem, tāpēc laiku pa laikam tie
                    jāizstaipa. Zem darba galda pacel gaisā vienu kāju un iztaisno to. Kustinot potīti, vispirms tuvini
                    kāju pirkstus sev, bet pēc tam noliec pēdu prom no sevis. Atkārto vingrinājumu desmit reizes ar
                    katru kāju. Noslēgumā apļo kāju pirkstus pulksteņa rādītāja virzienā un pēc tam pretēji pulksteņa
                    rādītāja virzienam.
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                      method="post">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg"
                                   placeholder="Lūdzu, ievadi e-pasta adresi..." required name="email">
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

</body>

</html>
