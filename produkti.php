<!DOCTYPE html>
<html lang="en">
<?php $title = 'Produkti'; ?>
<?php $currentPage = 'produkti'; ?>
<?php include('navbar.php'); ?>
<?php
$allProducts = \aptieka\models\Product::getAll();
$count = count($allProducts);
?>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $product = New \aptieka\controllers\ActionController();
    $product->addSubs();
}
?>
<body>

<!-- Masthead -->
<header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <img src="img/logo_dabas_aptieka.png">
                <h1 class="mb-5">Produkti</h1>
            </div>
        </div>
    </div>
</header>

<!-- Icons Grid -->
<section class="features-icons bg-light text-center">
    <div class="container-fluid">
        <div class="row">
            <?php foreach ($allProducts as $key => $product) { ?>
                <div class="col-lg-3">
                    <div class="card m-2" style="height: 500px!important;">
                        <img class="card-img-top mx-auto" src="<?php echo $allProducts[$key]['picture'] ?>"
                             alt="Card image" style="width:50%; height: 250px!important;">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $allProducts[$key]['name'] ?></h4>
                            <p class="card-text"><?php echo $allProducts[$key]['category'] ?></p>
                            <p class="card-text">Cena: € <?php echo $allProducts[$key]['price'] ?></p>
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#myModal<?php echo $key
                            ?>">DETALIZĒTI</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<!-- Call to Action -->
<section class="call-to-action text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h2 class="mb-4">Piesakies jaunumiem!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form action="
                <?php
                echo htmlspecialchars($_SERVER["PHP_SELF"]); //Submit form to itself
                ?>"
                      method="post">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="email" class="form-control form-control-lg"
                                   placeholder="Lūdzu, ievadi e-pasta adresi..." required name="email">
                        </div>
                        <div class="col-12 col-md-3">
                            <button class="btn btn-block btn-lg btn-success">Pieteikties!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php foreach ($allProducts as $key => $product) { ?>
    <div class="modal fade" id="myModal<?php echo $key ?>">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">DETALIZĒTI</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <div class="card">
                                <img class="card-img-top mx-auto"
                                     src="<?php echo $allProducts[$key]['picture'] ?>" alt="Card image"
                                     style="width:50%">
                                <div class="card-body">
                                    <h4 class="card-title text-center"><?php echo $allProducts[$key]['name'] ?></h4>
                                    <p class="card-text text-center"><?php if ($allProducts[$key]['prescription'] == 0) echo 'BEZRECEPŠU MEDIKAMENTS'; else echo 'RECEPŠU MEDIKAMENTS'; ?></p>
                                    <p class="card-text text-center font-weight-bold"><?php echo $allProducts[$key]['about'] ?></p>
                                    <p style="display: none" id="id"> <?php echo $allProducts[$key]['price'] ?>   </p>

                                    <h5 class="card-text font-weight-bold text-center">Cena:
                                        € <?php echo $allProducts[$key]['price'] ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <?php
                    if ($ac == 1) {
                        echo '<div class="count" style="opacity: 0" id="prid' . $key . '" >' . $allProducts[$key]['id'] . '</div>';
                        echo '<div class="user" style="opacity: 0" >' . $_SESSION['user'] . '</div>'

                        ?>
                        <button type="button" class="btn btn-success mr-auto add" data-dismiss="modal">PIEVIENOT
                            GROZAM
                        </button>


                        <?php
                    } ?>
                    <button type="button" class="btn btn-secondary ml-auto" data-dismiss="modal">AIZVĒRT</button>
                </div>

            </div>
        </div>
    </div>
<?php } ?>
<?php include('footer.php'); ?>

</body>

</html>
